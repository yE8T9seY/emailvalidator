package tests;


/*
 * 
 * 
 * Anuj Choudhary
 * 991539753
 * 
 * 
 */


import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.EmailValidator;

public class EmailValidatorTest {

	
	//  ----------------isValidEmailFormat-----------------
	
	@Test
	public final void testIsValidFormatRegular() {
		String email="anujChoudhary268@gmail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testIsValidFormatException() {
		String email= "";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertFalse("The emai provided is not valid ",result);
	}

	@Test
	public final void testIsValidFormatBoundaryIn() {
		String email="an@gmail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testIsValidFormatBoundaryOut() {
		String email="anujChoudhary268gmail.com";
		boolean result=EmailValidator.isValidEmailFormat(email);
		assertFalse("The emai provided in not valid ",result);
	}

	

	
	
	//  ----------------hasOnlyOneAtSign-----------------
	
	@Test
	public final void testHasOnlyOneAtSignRegular() {
		String email="anujChoudhary268@gmail.com";
		boolean result=EmailValidator.hasOnlyOneAtSign(email);
		assertTrue("The email provided is not valid",result);
	}



	@Test
	public final void testHasOnlyOneAtSignBoundaryIn() {
		String email="anaw12312@gmail.com";
		boolean result=EmailValidator.hasOnlyOneAtSign(email);
		assertTrue("The email provided is not valid",result);
	}

	@Test
	public final void testHasOnlyOneAtSignBoundaryOut() {
		String email="anujChoudhary268@@gmail.com";
		boolean result=EmailValidator.hasOnlyOneAtSign(email);
		assertFalse("The emai provided is not valid ",result);
	}

	
	
	
	
	//  ----------------hasThreeLowerAlpha-----------------
	
	@Test
	public final void testHasThreeLowerAlphaRegular() {
		String email="anujChoudhary268@gmail.com";
		boolean result=EmailValidator.hasThreeLowerAlpha(email);
		assertTrue("The email provided is valid",result);
	}

	@Test
	public final void testHasThreeLowerAlphaException() {
		String email= "ASDBSKADSN";
		boolean result=EmailValidator.hasThreeLowerAlpha(email);
		assertFalse("The emai provided is not valid",result);
	}

	@Test
	public final void testHasThreeLowerAlphaBoundaryIn() {
		String email="tesTING@gmail.com";
		boolean result=EmailValidator.hasThreeLowerAlpha(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasThreeLowerAlphaBoundaryOut() {
		String email="teSTING@gmail.com";
		boolean result=EmailValidator.hasThreeLowerAlpha(email);
		assertFalse("The emai provided has not valid format",result);
	}

	
	
	
	
	//  ----------------hasAtleastThreeAlphaInDomain-----------------
	
	@Test
	public final void testHasAtleastThreeAlphaInDomainRegular() {
		String email="testMail@gmail23.com";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasAtleastThreeAlphaInDomainException() {
		String email= "";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertFalse("The emai provided has not valid format",result);
	}

	@Test
	public final void testHasAtleastThreeAlphaInDomainBoundaryIn() {
		String email="an@don.com";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasAtleastThreeAlphaInDomainBoundaryOut() {
		String email="anuj@te.com";
		boolean result=EmailValidator.hasAtleastThreeAlphaInDomain(email);
		assertFalse("The emai provided has not valid format",result);
	}

	
	
		
	//  ----------------hasTwoAlphaInExtension-----------------
	
	@Test
	public final void testHasTwoAlphaInExtensionRegular() {
		String email="anujChoudhary268@gmail.com";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasTwoAlphaInExtensionException() {
		String email= "";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertFalse("The emai provided has not valid format",result);
	}

	@Test
	public final void testHasTwoAlphaInExtensionBoundaryIn() {
		String email="an@gmail.co";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertTrue("The email provided has valid format",result);
	}

	@Test
	public final void testHasTwoAlphaInExtensionBoundaryOut() {
		String email="anujChoudhary268gmail.c";
		boolean result=EmailValidator.hasTwoAlphaInExtension(email);
		assertFalse("The emai provided has not valid format",result);
	}

}
