package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
	
	/*
	 * 
	 * Anuj Choudhary
	 * 991539753 
	 * 
	 */
	
	public static String regex = "^(.+)@(.+)$";
	 
	public static Pattern pattern = Pattern.compile(regex);
	
	//     1.	Email must be in this format: <account>@<domain>.<extension>
	public static boolean isValidEmailFormat(String emailEntered)
	{
		 Matcher matcher = pattern.matcher(emailEntered);
		return matcher.matches();
	}

	//    2.	Email should have one and only one @ symbol.
	public static boolean hasOnlyOneAtSign(String emailEntered)
	{
		int count =0;
		
        for (int i = 0; i < emailEntered.length(); i++) {
        
        	if(count==2)
        	{
        		return false;
        	}
        	if(emailEntered.charAt(i)=='@')
        	{
        		count++;
        	}
        }
        
		return true;
	}

	//     3.	Account name should have at least 3 alpha-characters in lowercase (must not start with a number).
	public static boolean hasThreeLowerAlpha(String emailEntered)
	{
		int count =0;
		int accountNameLength =0 ;
		
        for (int i = 0; i < emailEntered.length(); i++) {
        	
        	
        	if(emailEntered.charAt(i)=='@')
        	{
        		break;
        	}
        	else
        	{
        		accountNameLength++;
        	}
        	
        }
        
for (int i = 0; i < accountNameLength; i++) {
        	
        
        	if(Character.isLowerCase(emailEntered.charAt(i)))
        	{
        		count++;
        	}
        }
        
        if(count<3)
        {
        	return false;
        }
        else
        {
        	return true;
        }
        
	}

	
	//      4.	Domain name should have at least 3 alpha-characters in lowercase or numbers.	
	public static boolean hasAtleastThreeAlphaInDomain(String emailEntered)
	{
		int accountNameLength =0 ;
		int alphaCount =0 ;
		
        for (int i = 0; i < emailEntered.length(); i++) {
        	
        	
        	if(emailEntered.charAt(i)=='@')
        	{
        		break;
        	}
        	else
        	{
        		accountNameLength++;
        	}
        	
        }
        
        for (int i = accountNameLength+1; i < emailEntered.length(); i++) {
        	if(emailEntered.charAt(i)=='.')
        	{
        		break;
        	}
        	else
        	{
        		if(Character.isLowerCase(emailEntered.charAt(i)) || Character.isDigit(emailEntered.charAt(i))) {
        			alphaCount++;
        	}
        	}
        	
        }
        if(alphaCount<3)
        {
        	return false;
        }
        else
        {
        	return true;
        }
        
	}
	
	//   5. Extension name should have at least 2 alpha-characters (no numbers).
	public static boolean hasTwoAlphaInExtension(String emailEntered)
	{
		
		int accountAndDomainLength =0 ;
		int alphaCount =0 ;
		
        for (int i = 0; i < emailEntered.length(); i++) {
        	
        	
        	if(emailEntered.charAt(i)=='.')
        	{
        		break;
        	}
        	else
        	{
        		accountAndDomainLength++;
        	}
        	
        }
        
      for (int i = accountAndDomainLength+1; i < emailEntered.length(); i++) {
        	
        	
        	if(Character.isDigit(emailEntered.charAt(i)))
        	{
        		break;
        	}
        	else
        	{
        		if(Character.isAlphabetic(emailEntered.charAt(i))) {
        			alphaCount++;
        		}
        		else
        		{
        			break;
        		}
        	}
        	
        }
      
      if(alphaCount<2)
      {
    	  return false;
      }
      else
      {
    	  return true;
      }
		
	}

	
	
	
}
